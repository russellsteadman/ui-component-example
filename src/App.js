import React from 'react';
import Classy from './shared/Classy';

const cx = Classy([]);

function App() {
  return (
    <div className={cx('container', 'py-4')}>
      <h1>UI Components</h1>
      <hr />
      <div>
        <h2>Button Component</h2>
        <p>Select a button color scheme that best fits the app (more colors and designs can be made depending upon button usage).</p>
        <div className={cx('btn', 'btn-primary', 'mx-2')}>Style 1</div>
        <div className={cx('btn', 'btn-reddish', 'mx-2')}>Style 2</div>
        <div className={cx('btn', 'btn-seablue', 'mx-2')}>Style 3</div>
        <div className={cx('btn', 'btn-outline-seablue', 'mx-2')}>Style 4</div>
        <div className={cx('btn', 'btn-outline-dark', 'mx-2')}>Style 5</div>
        <p className={cx('mt-3')}>Select a button rounding that best fits the app.</p>
        <div className={cx('btn', 'btn-dark', 'mx-2')}>Style 1</div>
        <div className={cx('btn', 'btn-dark', 'rounded-1', 'mx-2')}>Style 2</div>
        <div className={cx('btn', 'btn-dark', 'rounded-2', 'mx-2')}>Style 3</div>
        <div className={cx('btn', 'btn-dark', 'rounded-3', 'mx-2')}>Style 4</div>
        <p className={cx('mt-3')}>Select a button size that best fits the app.</p>
        <div className={cx('btn', 'btn-dark', 'mx-2')}>Style 1</div>
        <div className={cx('btn', 'btn-dark', 'size-1', 'mx-2')}>Style 2</div>
        <div className={cx('btn', 'btn-dark', 'size-2', 'mx-2')}>Style 3</div>
        <div className={cx('btn', 'btn-dark', 'size-3', 'mx-2')}>Style 4</div>
      </div>
    </div>
  );
}

export default App;
